using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Figures
{
    /// <summary>
    /// ����������� ��������� �������� ����� � ���� �����.
    /// </summary>
    public interface IFigureSettings { }

    public struct ParallelepipedSettings : IFigureSettings
    {
        public float Length;
        public float Width;
        public float Height;
    }
    public struct SphereSettings : IFigureSettings
    {
        public float Radius;
        public int SegmentCount;
    }
    public struct PrismSettings : IFigureSettings
    {
        public float Height;
        public float Radius;
        public int CountEdge;
    }
    public struct CapsuleSettings : IFigureSettings
    {
        public float Radius;
        public float Height;
        public int SegmentCount;
    }
}
