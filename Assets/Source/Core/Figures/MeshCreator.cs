using System;
using System.Collections.Generic;
using UnityEngine;

namespace Figures
{
    /// <summary>
    /// ������� ��� �������� ���� �� ����������� ���� ������.
    /// </summary>
    static class MeshCreator
    {
        /// <summary>
        /// ������� �������������� ��������� ���� � ������-�����������.
        /// </summary>
        private static Dictionary<Type, Func<IFigureSettings, Mesh>> MethodsCreating
            = new Dictionary<Type, Func<IFigureSettings, Mesh>>()
            {
                [typeof(ParallelepipedSettings)] = settings => CreateParallelepiped((ParallelepipedSettings)settings),
                [typeof(SphereSettings)] = settings => CreateSphere((SphereSettings)settings),
                [typeof(PrismSettings)] = settings => CreatePrism((PrismSettings)settings),
                [typeof(CapsuleSettings)] = settings => CreateCapsule((CapsuleSettings)settings),
            };

        public static Mesh Create(IFigureSettings settings)
            => MethodsCreating[settings.GetType()](settings);

        /// <summary>
        /// ����� �������� ���������������.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        private static Mesh CreateParallelepiped(ParallelepipedSettings settings)
        {
            var builder = new MeshBuilder("Parallelepiped");

            var up = Vector3.up * settings.Height;
            var right = Vector3.right * settings.Width;
            var forward = Vector3.forward * settings.Length;

            var backCorner = new Vector3(-right.x / 2, -up.y / 2, -forward.z / 2);
            var frontCorner = -backCorner;


            builder.BuildQuad(backCorner, right, up); //back
            builder.BuildQuad(backCorner, up, forward); //left
            builder.BuildQuad(backCorner, forward, right); //down

            builder.BuildQuad(frontCorner, -up, -right); //front
            builder.BuildQuad(frontCorner, -forward, -up); //right
            builder.BuildQuad(frontCorner, -right, -forward); //up


            return builder.Create();
        }

        /// <summary>
        /// ����� �������� �����.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        private static Mesh CreateSphere(SphereSettings settings)
        {
            var radius = settings.Radius;
            var segmentCount = settings.SegmentCount;
            var heightSegmentCount = segmentCount / 2;

            var angleInc = Mathf.PI / heightSegmentCount;

            var builder = new MeshBuilder("Sphere");


            for (int i = 0; i <= heightSegmentCount; i++)
            {
                var center = Vector3.zero;
                center.y += -Mathf.Cos(angleInc * i) * radius;

                var currentRadius = Mathf.Sin(angleInc * i) * radius;

                float v = (float)i / heightSegmentCount;

                builder.BuildRingForSphere(segmentCount, center, currentRadius, v, i > 0);
            }

            return builder.Create();
        }

        /// <summary>
        /// ����� �������� ������.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        private static Mesh CreatePrism(PrismSettings settings)
        {
            var height = settings.Height;
            var radius = settings.Radius;
            int countEdge = settings.CountEdge;

            var angleInc = (2f * Mathf.PI) / countEdge;
            var downVertex = new Vector3[countEdge + 1];
            var upVertex = new Vector3[countEdge + 1];

            for (int i = 0; i <= countEdge; i++)
            {
                var angle = angleInc * i;
                downVertex[i] = new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle)) * radius;
                upVertex[i] = new Vector3(downVertex[i].x, height, downVertex[i].z);
            }

            var builder = new MeshBuilder("Prism");

            for (int i = 0; i < downVertex.Length - 1; i++)
            {
                var corner1 = downVertex[i];
                var corner2 = upVertex[i];
                var corner3 = downVertex[i + 1];

                builder.BuildQuad(corner1, corner3 - corner1, corner2 - corner1);
            }

            builder.BuildCap(Vector3.up * height, countEdge, radius);
            builder.BuildCap(Vector3.zero, countEdge, radius, false);

            return builder.Create();
        }

        /// <summary>
        /// ����� �������� �������.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        private static Mesh CreateCapsule(CapsuleSettings settings)
        {
            var builder = new MeshBuilder("Capsule");

            var height = settings.Height;
            var radius = settings.Radius;
            if (height < 2 * radius) height = 2 * radius;
            
            var heightSegmentCount = 3; //��������������� � ������.
            var radialSegmentCount = settings.SegmentCount; 

            var heightCylinderBody = height - 2 * radius;
            var downCornerCylinder = new Vector3(0, -heightCylinderBody / 2, 0);


            CreateDownHemisphere(builder, Vector3.zero + Vector3.down * heightCylinderBody / 2, radialSegmentCount, radius);
            CreateCylinderBody(builder, downCornerCylinder, heightCylinderBody, 
                radius, heightSegmentCount, radialSegmentCount);
            CreateUpHemisphere(builder, Vector3.zero + Vector3.up * heightCylinderBody / 2, radialSegmentCount, radius);

            return builder.Create();
        }

        private static void CreateDownHemisphere(MeshBuilder builder, Vector3 center,
            int radialSegmentCount, float radius)
        {
            var heightSegmentCount = radialSegmentCount / 2;
            if (heightSegmentCount % 2 != 0) heightSegmentCount--;

            var halfHeightSegmentCount = heightSegmentCount / 2;

            var angleInc = Mathf.PI / heightSegmentCount;
            
            for (int i = 0; i < halfHeightSegmentCount; i++)
            {
                var currentCenter = center;
                currentCenter.y += -Mathf.Cos(angleInc * i) * radius;

                var currentRadius = Mathf.Sin(angleInc * i) * radius;

                var v = (float)i / heightSegmentCount;

                builder.BuildRingForSphere(radialSegmentCount, currentCenter, currentRadius, v, i > 0);
            }
        }

        private static void CreateUpHemisphere(MeshBuilder builder, Vector3 center,
            int radialSegmentCount, float radius)
        {
            var heightSegmentCount = radialSegmentCount / 2;
            if (heightSegmentCount % 2 != 0) heightSegmentCount--;

            var halfHeightSegmentCount = heightSegmentCount / 2;

            var angleInc = Mathf.PI / heightSegmentCount;

            for (int i = halfHeightSegmentCount + 1; i <= heightSegmentCount; i++)
            {
                var currentCenter = center;
                currentCenter.y += -Mathf.Cos(angleInc * i) * radius;
                var currentRadius = Mathf.Sin(angleInc * i) * radius;

                float v = (float)i / heightSegmentCount;

                builder.BuildRingForSphere(radialSegmentCount, currentCenter, currentRadius, v, true);
            }
        }

        private static void CreateCylinderBody(MeshBuilder builder, Vector3 downCorner, float height, float radius,
            int heightSegmentCount, int radialSegmentCount)
        {
            var heightInc = height / heightSegmentCount;

            for (int i = 0; i <= heightSegmentCount; i++)
            {
                var center = downCorner + Vector3.up * heightInc * i;
                var v = (float)i / heightSegmentCount;

                builder.BuildRing(radialSegmentCount, center, radius, v, true);
            }
        }
    }
}
