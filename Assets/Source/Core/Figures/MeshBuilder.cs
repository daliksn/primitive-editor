using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Figures
{

    /// <summary>
    /// ������ "���������" ��� ������� ��������� ����.
    /// </summary>
    class MeshBuilder
    {
        private List<Vector3> _vertices;
        private List<Vector3> _normals;
        private List<Vector2> _uv;
        private List<int> _indexes;

        string name;

        public MeshBuilder(string name)
        {
            _vertices = new List<Vector3>();
            _normals = new List<Vector3>();
            _uv = new List<Vector2>();
            _indexes = new List<int>();

            this.name = name;
        }

        public IReadOnlyCollection<Vector3> Vertices => _vertices;
        public IReadOnlyCollection<Vector3> Normals => _normals;
        public IReadOnlyCollection<Vector2> UV => _uv;
        public IReadOnlyCollection<int> Triangles => _indexes;

        public Mesh Create()
        {
            var mesh = new Mesh()
            {
                vertices = _vertices.ToArray(),
                triangles = _indexes.ToArray(),
                normals = _normals.ToArray(),
                uv = _uv.ToArray(),
                name = this.name,
            };

            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            return mesh;
        }
        
        /// <summary>
        /// ����� ���������� ����� ������� � �������� � UV-�����������, ����������� � �������.
        /// </summary>
        /// <param name="vertice"></param>
        /// <param name="normal"></param>
        /// <param name="UV"></param>
        public void AddVertices(Vector3 vertice, Vector3 normal, Vector2 UV)
        {
            _vertices.Add(vertice);
            _normals.Add(normal);
            _uv.Add(UV);
        }

        /// <summary>
        /// �������� "������������" ����� ���������� �������� ������.
        /// </summary>
        /// <param name="index0"></param>
        /// <param name="index1"></param>
        /// <param name="index2"></param>
        public void AddTriangle(int index0, int index1, int index2)
        {
            _indexes.Add(index0);
            _indexes.Add(index1);
            _indexes.Add(index2);
        }

        /// <summary>
        /// ����� ��� �������� �������� ��������.
        /// </summary>
        /// <param name="corner"></param>
        /// <param name="widthDir"></param>
        /// <param name="lengthDir"></param>
        public void BuildQuad(Vector3 corner, Vector3 widthDir, Vector3 lengthDir)
        {
            var normal = Vector3.Cross(lengthDir, widthDir).normalized;

            AddVertices(corner, normal, Vector2.zero);
            AddVertices(corner + lengthDir, normal, Vector2.up);
            AddVertices(corner + lengthDir + widthDir, normal, Vector2.one);
            AddVertices(corner + widthDir, normal, Vector2.right);
            
            var baseIndex = _vertices.Count - 4;

            AddTriangle(baseIndex, baseIndex + 1, baseIndex + 2);
            AddTriangle(baseIndex, baseIndex + 2, baseIndex + 3);
        }

        /// <summary>
        /// ����� ��� �������� �������� ������ �� ������.
        /// </summary>
        /// <param name="segmentCount"></param>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <param name="v"></param>
        /// <param name="buildTriangles"></param>
        public void BuildRing(int segmentCount, Vector3 center, float radius, float v, bool buildTriangles)
        {
            var angleInc = (2f * Mathf.PI) / segmentCount;

            for (int i = 0; i <= segmentCount; i++)
            {
                var angle = angleInc * i;

                var unitPosition = new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle));

                AddVertices(center + unitPosition * radius, 
                    unitPosition.normalized, new Vector2((float)i / segmentCount, v));

                if (i > 0 && buildTriangles)
                {
                    var baseIndex = _vertices.Count - 1;
                    var vertsPerRow = segmentCount + 1;

                    var index0 = baseIndex;
                    var index1 = baseIndex - 1;
                    var index2 = baseIndex - vertsPerRow;
                    var index3 = baseIndex - vertsPerRow - 1;

                    AddTriangle(index0, index2, index1);
                    AddTriangle(index2, index3, index1);
                }
            }
        }

        /// <summary>
        /// ����� ��� �������� ������ �� ������ � ��������� ��� �����.
        /// </summary>
        /// <param name="segmentCount"></param>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <param name="v"></param>
        /// <param name="buildTriangles"></param>
        public void BuildRingForSphere(int segmentCount, Vector3 center, float radius, float v, bool buildTriangles)
        {
            float angleInc = (Mathf.PI * 2.0f) / segmentCount;

            for (int i = 0; i <= segmentCount; i++)
            {
                float angle = angleInc * i;

                Vector3 unitPosition = Vector3.zero;
                unitPosition.x = Mathf.Cos(angle);
                unitPosition.z = Mathf.Sin(angle);

                Vector3 vertexPosition = center + unitPosition * radius;

                AddVertices(vertexPosition, (vertexPosition - center).normalized, new Vector2((float)i / segmentCount, v));

                if (i > 0 && buildTriangles)
                {
                    int baseIndex = _vertices.Count - 1;

                    int vertsPerRow = segmentCount + 1;

                    int index0 = baseIndex;
                    int index1 = baseIndex - 1;
                    int index2 = baseIndex - vertsPerRow;
                    int index3 = baseIndex - vertsPerRow - 1;

                    AddTriangle(index0, index2, index1);
                    AddTriangle(index2, index3, index1);
                }
            }
        }

        /// <summary>
        /// �������� ���������, ��������� �� �������� ������ � ����������� ������� 
        /// ��� ���������� ������������� n-������ ������.
        /// </summary>
        /// <param name="center"></param>
        /// <param name="segmentCount"></param>
        /// <param name="radius"></param>
        /// <param name="isDirectionUp"></param>
        public void BuildCap(Vector3 center,
            int segmentCount, float radius, bool isDirectionUp = true)
        {
            var normal = isDirectionUp ? Vector3.up : Vector3.down;

            AddVertices(center, normal, new Vector2(0.5f, 0.5f));

            var centerVertexIndex = _vertices.Count - 1;
            var angleInc = (Mathf.PI * 2.0f) / segmentCount;

            for (int i = 0; i <= segmentCount; i++)
            {
                var angle = angleInc * i;

                var vertex = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));

                _vertices.Add(center + vertex * radius);
                _normals.Add(normal);
                _uv.Add(new Vector2(vertex.x + 1.0f, vertex.z + 1.0f) * 0.5f);

                if (i > 0)
                {
                    var baseIndex = _vertices.Count - 1;

                    if (isDirectionUp)
                        AddTriangle(centerVertexIndex, baseIndex, baseIndex - 1);
                    else
                        AddTriangle(centerVertexIndex, baseIndex - 1, baseIndex);

                }
            }
        }
    }
}