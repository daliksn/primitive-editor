using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraBehaviour;

namespace Figures 
{
    /// <summary>
    /// �������� ��� ����������� ������ � ����� ������.
    /// </summary>
    public class FigureDetector
    {
        CameraController _controller;

        public FigureDetector(CameraController controller) => _controller = controller;

        public Figure Detect(Vector3 screenPosition)
        {
            var ray = _controller.Camera.ScreenPointToRay(screenPosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var collider = hit.collider;
                return collider.GetComponent<Figure>();
            }

            return null;
        }
    }
}