using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Button))]
    public class ChoiseColorContainer : MonoBehaviour
    {
        [SerializeField] Color color;

        private void Awake()
        {
            var button = gameObject.GetComponent<Button>();
            button.onClick.AddListener(() => IsClicked = true);
        }

        public bool IsClicked { get; set; }
        public Color Color => color;

    }
}