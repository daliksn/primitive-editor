using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using Figures;

namespace UI
{
    /// <summary>
    /// ����� ������ ���� ������.
    /// </summary>
    public class ChoiseFigureForm : MonoBehaviour
    {
        ChoiseFigureContainer[] _containers;

        static Dictionary<ChoiseFigureContainer.Type, Type> ConvertorTypesFigure = new Dictionary<ChoiseFigureContainer.Type, Type>()
        {
            [ChoiseFigureContainer.Type.Parallelepiped] = typeof(ParallelepipedSettings),
            [ChoiseFigureContainer.Type.Sphere] = typeof(SphereSettings),
            [ChoiseFigureContainer.Type.Prism] = typeof(PrismSettings),
            [ChoiseFigureContainer.Type.Capsule] = typeof(CapsuleSettings),
        };

        private void Awake()
        {
            _containers = gameObject.GetComponentsInChildren<ChoiseFigureContainer>(true);
        }

        public async UniTask<Type> GetTypeFigure()
        {
            gameObject.SetActive(true);
            Type result = null;

            foreach (var container in _containers) container.IsClicked = false;

            while (true)
            {
                await UniTask.Yield();

                foreach (var contianer in _containers)
                {
                    if (contianer.IsClicked)
                    {
                        result = ConvertorTypesFigure[contianer.TypeFigure];
                        break;
                    }
                }

                if (result != null) break;
            }


            gameObject.SetActive(false);
            return result;
        }
    }
}