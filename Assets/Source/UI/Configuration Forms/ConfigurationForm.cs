using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using Figures;

namespace UI
{

    public abstract class ConfigurationForm : MonoBehaviour, IConfigurationForm
    {
        [SerializeField] Button _completeButton;

        public async UniTask<IFigureSettings> Getsettings()
        {
            var isComplete = false;
            _completeButton.onClick.AddListener(() => isComplete = true);

            gameObject.SetActive(true);

            while (true)
            {
                await UniTask.Yield();

                if (isComplete)
                {
                    _completeButton.onClick.RemoveAllListeners();
                    gameObject.SetActive(false);
                    return CreateSettings();
                }
            }
        }

        protected abstract IFigureSettings CreateSettings();
    }
}