using UnityEngine;
using UnityEngine.UI;
using Editing;
using Figures;

/// <summary>
/// ����� �����. 
/// ��������� �������� ������ �������.
/// </summary>
public class Startup : MonoBehaviour
{
    [SerializeField] Figure _prefabFigure;
    [SerializeField] Button _figureCreationButton;

    EditorController _editor;

    private void Awake()
    {
        var controller = Object.FindObjectOfType<CameraBehaviour.CameraController>();

        var detector = new FigureDetector(controller);
        var factory = new Figure.Factory(_prefabFigure);

        _editor = new EditorController(factory, detector, controller, _figureCreationButton);
    }

    private void Start() => _editor.TransferControl();
}
