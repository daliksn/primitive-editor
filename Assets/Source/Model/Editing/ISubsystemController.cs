using Cysharp.Threading.Tasks;
namespace Editing
{

    /// <summary>
    /// ��������� ����������� ����������.
    /// </summary>
    public interface ISubsystemController
    {
        /// <summary>
        /// �������� ��������� ��������� ����������.
        /// </summary>
        /// <returns></returns>
        public UniTask TransferControl();
    }

    public interface ISubsystemController<T>
    {
        public UniTask TransferControl(T data);
    }
}
