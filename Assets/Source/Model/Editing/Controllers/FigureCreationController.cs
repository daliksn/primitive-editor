using System;
using UnityEngine;
using Cysharp.Threading.Tasks;
using Figures;
using UI;

namespace Editing
{
    /// <summary>
    /// ���������� ���������� �������� �������� �����.
    /// </summary>
    class FigureCreationController : ISubsystemController<Vector3>
    {
        ChoiseFigureForm _choiseFigureForm;
        ChoiseColorForm _choiseColorForm;
        Figure.Factory _factory;
        SettingsConstructor _constructor;

        public FigureCreationController(Figure.Factory factory)
        {
            _factory = factory;

            _choiseFigureForm = GameObject.FindObjectOfType<ChoiseFigureForm>(true);
            _choiseColorForm = GameObject.FindObjectOfType<ChoiseColorForm>(true);
            _constructor = new SettingsConstructor();
        }

        public async UniTask TransferControl(Vector3 position)
        {
            var type = await _choiseFigureForm.GetTypeFigure();
            var settings = await _constructor.CreateSettings(type);
            var color = await _choiseColorForm.GetColor();

            _factory.Create(settings, position, color);
        }
    }

    /// <summary>
    /// ����������� �������� ���������� ������ ����� UI.
    /// </summary>
    class SettingsConstructor
    {
        IConfigurationForm _parallelepipedForm;
        IConfigurationForm _sphereForm;
        IConfigurationForm _prismForm;
        IConfigurationForm _capsuleForm;

        public SettingsConstructor()
        {
            _parallelepipedForm = GameObject.FindObjectOfType<ParallelepipedConfigurationForm>(true);
            _sphereForm = GameObject.FindObjectOfType<SphereConfigurarionForm>(true);
            _prismForm = GameObject.FindObjectOfType<PrismConfigurationForm>(true);
            _capsuleForm = GameObject.FindObjectOfType<CapsuleConfigurationForm>(true);
        }

        public async UniTask<IFigureSettings> CreateSettings(Type type)
        {
            if (type == typeof(ParallelepipedSettings)) return await _parallelepipedForm.Getsettings();
            else if (type == typeof(SphereSettings)) return await _sphereForm.Getsettings();
            else if (type == typeof(PrismSettings)) return await _prismForm.Getsettings();
            else if (type == typeof(CapsuleSettings)) return await _capsuleForm.Getsettings();
            else throw new Exception();
        }
    }
}
