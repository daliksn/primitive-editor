using UnityEngine;
using Cysharp.Threading.Tasks;
using CameraBehaviour;

namespace Editing {

    /// <summary>
    /// ���������� ���������� �������� ������ ������.
    /// </summary>
    class CameraFlightController : ISubsystemController
    {
        CameraController _camera;

        public CameraFlightController(CameraController camera) => _camera = camera;

        public async UniTask TransferControl()
        {
            if (Input.GetKeyUp(KeyCode.Mouse1)) return;

            CursorUtility.MousePosition position;
            CursorUtility.GetCursorPos(out position);

            Cursor.lockState = CursorLockMode.Locked;
            _camera.EnableFlightMode();

            while (true)
            {
                await UniTask.Yield();

                if (Input.GetKeyUp(KeyCode.Mouse1))
                {
                    _camera.DisableFlightMode();
                    Cursor.lockState = CursorLockMode.None;
                    CursorUtility.SetCursorPos(position.x, position.y);
                    break;
                }
            }
        }
    }
}